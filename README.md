# GoHugo Kroki

Go Hugo [Kroki!](https://kroki.io) diagram rendering support.

- Layout Short code [kroki-viewer](layouts/shortcodes/kroki-viewer.html)
- Diagram-type *Markup Inline Code Block Rendering* Layouts
  - [x] [BPMN](layouts/_default/_markup/render-codeblock-bpmn.html)
  - [x] [Ditaa](layouts/_default/_markup/render-codeblock-ditaa.html)
  - [x] [Excalidraw](layouts/_default/_markup/render-codeblock-excalidraw.html)
  - [x] [Mermaid](layouts/_default/_markup/render-codeblock-mermaid.html)
  - [x] [PlantUML](layouts/_default/_markup/render-codeblock-plantuml.html)

Bronnen:

- [kroki!](https://kroki.io)
- [Speciale HTML symbolen](https://cheatography.com/davechild/cheat-sheets/html-character-entities/)

Test
