# Kroki! Test

Test-publicatie *kroki!* Layout voor weergave via de **HUGO** Sitegenerator.

## Diagram PlantUML-tekst

In *Markdown*-content kan een inline-blok worden opgenomen met een door de *kroki!* Layout ondersteunde diagram tekst definitie. Zie voorbeeld hieronder.

>&#96;&#96;&#96;plantuml  
@startuml  
"<b>HUGO</b> Sitegenerator" -> "<i>kroki!</i> Layout" :  tekst-codering  
"<i>kroki!</i> Layout" -> "<i>kroki!</i> Server" : opvragen weergave  
"<i>kroki!</i> Layout" <- "<i>kroki!</i> Server" : ontvangen weergave  
"<b>HUGO</b> Sitegenerator" <- "<i>kroki!</i> Layout": diagram weergave  
@enduml  
&#96;&#96;&#96;  

## Diagram SVG-weergave

De **HUGO** Sitegenerator leest de *Markdown*-content en vraagt op basis van de *kroki!* Layout een weergave op bij de *kroki!* Server[^1]. De

```plantuml
@startuml
"<b>HUGO</b> Sitegenerator" -> "<i>kroki!</i> Layout" :  tekst-codering (PlantUML)
"<i>kroki!</i> Layout" -> "<i>kroki!</i> Server" : opvragen weergave (Post-Request)
"<i>kroki!</i> Layout" <- "<i>kroki!</i> Server" : ontvangen weergave (Response)
"<b>HUGO</b> Sitegenerator" <- "<i>kroki!</i> Layout": diagram weergave (SVG)
@enduml
```

[^1]: Er word hierbij gebruik gemaakt van [*kroki!* Post-Request optie](https://kroki.io/#post-requests).