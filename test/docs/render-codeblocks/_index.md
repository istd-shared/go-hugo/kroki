# Codeblock Rendering

Gebruik van [Render Hooks for Code Blocks](https://gohugo.io/templates/render-hooks/) voor de weergave van *kroki!* diagrammen binnen een *Markdown* bestand.

{{< section >}}