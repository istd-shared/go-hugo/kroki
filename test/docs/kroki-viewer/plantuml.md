---
description: Voorbeeld PlantUML-diagram.
---

# PlantUML-rendering

PlanUML weergave vanuit digram-bestand.

## HUGO Layout Shortcode

De *kroki-viewer*-Shortcode kan worden gebruikt om een een diagram-bestand vanuit *Markdown*-content weer te geven. Zie onderstaande voorbeeld.

>&#123;&#123;< kroki-viewer  
&nbsp;&nbsp;&nbsp;file=test.plantuml  
&nbsp;&nbsp;&nbsp;type=plantuml  
&nbsp;&nbsp;&nbsp;name=testplantuml  
&#62;&#125;&#125;

## Diagram SVG-weergave

Weergave van *test.plantuml*.

{{< kroki-viewer
 file=test.plantuml
 type=plantuml
 name=testplantuml 
>}}

